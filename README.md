# myfirstproject
# Simple Django App
This is a simple, minimal Django app intended to help understand the main aspects of working with Django.

## Usage Instructions
```

pip install -r requirements.txt


```
Navigate to the 'cool_counters' Django project:

```
cd simple-django-app/cool_counters
```

Run migrations to update/create database
```
python manage.py migrate
```

Run the Django development server
```
python manage.py runserver 0.0.0.0:3030
```

Navigate to to http://server_ip:3030
